#Raul Baena Nocea C2950 F0C0546Y052
C2950 Boot Loader (CALHOUN-HBOOT-M) Version 12.0(5.3)WC(1), MAINTENANCE INTERIM SOFTWARE
Compiled Mon 30-Apr-01 07:56 by devgoyal
WS-C2950-24 starting...
Base ethernet MAC Address: 00:07:eb:cb:48:c0
Xmodem file system is available.
Initializing Flash...
flashfs[0]: 2 files, 1 directories
flashfs[0]: 0 orphaned files, 0 orphaned directories
flashfs[0]: Total bytes: 7741440
flashfs[0]: Bytes used: 1676800
flashfs[0]: Bytes available: 6064640
flashfs[0]: flashfs fsck took 5 seconds.
...done initializing flash.
Boot Sector Filesystem (bs:) installed, fsid: 3
Parameter Block Filesystem (pb:) installed, fsid: 4
Loading "flash:c2950-c3h2s-mz.120-5.3.WC.1.bin"...############################################################################################################

File "flash:c2950-c3h2s-mz.120-5.3.WC.1.bin" uncompressed and installed, entry point: 0x80010000
executing...

              Restricted Rights Legend

Use, duplication, or disclosure by the Government is
subject to restrictions as set forth in subparagraph
(c) of the Commercial Computer Software - Restricted
Rights clause at FAR sec. 52.227-19 and subparagraph
(c) (1) (ii) of the Rights in Technical Data and Computer
Software clause at DFARS sec. 252.227-7013.

           cisco Systems, Inc.
           170 West Tasman Drive
           San Jose, California 95134-1706



Cisco Internetwork Operating System Software 
IOS (tm) C2950 Software (C2950-C3H2S-M), Version 12.0(5.3)WC(1), MAINTENANCE INTERIM SOFTWARE
Copyright (c) 1986-2001 by cisco Systems, Inc.
Compiled Mon 30-Apr-01 07:56 by devgoyal
Image text-base: 0x80010000, data-base: 0x8031A000


Initializing flashfs...
flashfs[1]: 2 files, 1 directories
flashfs[1]: 0 orphaned files, 0 orphaned directories
flashfs[1]: Total bytes: 7741440
flashfs[1]: Bytes used: 1676800
flashfs[1]: Bytes available: 6064640
flashfs[1]: flashfs fsck took 5 seconds.
flashfs[1]: Initialization complete.
Done initializing flashfs.
C2950 POST: System Board Test : Passed
C2950 POST: Ethernet Controller Test : Passed
C2950 POST: MII TEST : Passed

cisco WS-C2950-12 (RC32300) processor (revision B0) with 22260K bytes of memory.
Processor board ID FOC0546Y05Z
Last reset from system-reset

Processor is running Enterprise Edition Software
Cluster command switch capable
Cluster member switch capable
12 FastEthernet/IEEE 802.3 interface(s)

32K bytes of flash-simulated non-volatile configuration memory.
Base ethernet MAC Address: 00:07:EB:CB:48:C0
Motherboard assembly number: 73-5782-08
Power supply part number: NONE
Motherboard serial number: FOC054603F6
Power supply serial number: DAB05411P8P
Model revision number: B0
Model number: WS-C2950-12
System serial number: FOC0546Y05Z
C2950 INIT: Complete

00:00:16: %SYS-5-RESTART: System restarted --
Cisco Internetwork Operating System Software 
IOS (tm) C2950 Software (C2950-C3H2S-M), Version 12.0(5.3)WC(1), MAINTENANCE INTERIM SOFTWARE
Copyright (c) 1986-2001 by cisco Systems, Inc.
Compiled Mon 30-Apr-01 07:56 by devgoyal

         --- System Configuration Dialog ---

At any point you may enter a question mark '?' for help.
Use ctrl-c to abort configuration dialog at any prompt.
Default settings are in square brackets '[]'.

Continue with configuration dialog? [yes/no]: no
Press RETURN to get started.
