EJERCICIO MIKROTIK 2.

OBJETIVO: CREAR DOS REDES WIFIS CON DISTINTOS TIPOS DE SEGURIDAD


 MIKROTIK
 +-------------------+
 |  1  2  3  4  wifi |
 +-------------------+
    |  |  |  |
    |  |  |  |
    |  |  |  +
    |  |  |  Conexión a un switch  con vlans
    |  |  +
    |  |  Interface de red placa base
    |  +
    |  Interface USB
    |  Ip 192.168.88.1XX/24
    +
    Punto de red del aula
    para salida a internet

## Pasos para conseguir tener 2 redes wifis separadas con distintos
niveles de seguridad.

Recordamos que:
- Solo disponemos de una interface wifi que trabaja en la banda de 2,4GHz.
- Este modelo de mikrotik dispone de 4 interfaces ethernet y 1 interface wifi
- En el PC dispondremos de una interface USB que es la que usaremos para
configurar el router por el puerto 2

Para conseguir dos redes wifi a partir de una sola interface hay que crear
sub-interfaces o interfaces virtuales. Cada interface virtual ha de estar 
asociada a un id de vlan (número de 1 a 2048)

Si queremos comunicarnos en la misma red entre un dispositivo wifi y un dispositivo
cableado hay que crear bridges entre la interface wifi y la interface ethernet.

### Eliminar configuración inicial
La configuración inicial que viene con la mikrotik podemos consultarla haciendo 
/export y obtenmos:

```
/interface bridge
add admin-mac=6C:3B:6B:30:B8:4F auto-mac=no comment=defconf name=bridge
/interface wireless
set [ find default-name=wlan1 ] band=2ghz-b/g/n channel-width=20/40mhz-Ce \
    disabled=no distance=indoors frequency=auto mode=ap-bridge ssid=\
    MikroTik-30B852 wireless-protocol=802.11
/interface ethernet
set [ find default-name=ether2 ] name=ether2-master
set [ find default-name=ether3 ] master-port=ether2-master
set [ find default-name=ether4 ] master-port=ether2-master
/ip neighbor discovery
set ether1 discover=no
set bridge comment=defconf
/ip pool
add name=default-dhcp ranges=192.168.88.10-192.168.88.254
/ip dhcp-server
add address-pool=default-dhcp disabled=no interface=bridge name=defconf
/interface bridge port
add bridge=bridge comment=defconf interface=ether2-master
add bridge=bridge comment=defconf interface=wlan1
/ip address
add address=192.168.88.1/24 comment=defconf interface=bridge network=\
    192.168.88.0
/ip dhcp-client
add comment=defconf dhcp-options=hostname,clientid disabled=no interface=ether1
/ip dhcp-server network
add address=192.168.88.0/24 comment=defconf gateway=192.168.88.1
/ip dns
set allow-remote-requests=yes
/ip dns static
add address=192.168.88.1 name=router
/ip firewall filter
add chain=input comment="defconf: accept ICMP" protocol=icmp
add chain=input comment="defconf: accept established,related" connection-state=\
    established,related
add action=drop chain=input comment="defconf: drop all from WAN" in-interface=\
    ether1
add action=fasttrack-connection chain=forward comment="defconf: fasttrack" \
    connection-state=established,related
add chain=forward comment="defconf: accept established,related" \
    connection-state=established,related
add action=drop chain=forward comment="defconf: drop invalid" connection-state=\
    invalid
add action=drop chain=forward comment="defconf:  drop all from WAN not DSTNATed" \
    connection-nat-state=!dstnat connection-state=new in-interface=ether1
/system routerboard settings
set boot-device=flash-boot cpu-frequency=650MHz protected-routerboot=disabled
/tool mac-server
set [ find default=yes ] disabled=yes
add interface=bridge
/tool mac-server mac-winbox
set [ find default=yes ] disabled=yes

```

A continuación modificamos todas esas partes de la configuración que no nos
interesan para dejar la mikrotik pelada, sin configuraciones iniciales
que interfieran con nuestra nueva programación del router:

```
# Cambiamos dirección IP para que la gestione directamente la interface 2
ip address set 0 interface=ether2-master

# Eliminamos el bridge

/interface bridge port remove 1
/interface bridge port remove 0
/interface bridge remove 0  

# Eliminamos que un puerto sea master de otro

/interface ethernet set [ find default-name=ether3 ] master-port=none
/interface ethernet set [ find default-name=ether4 ] master-port=none

# Cambiamos nombres a los puertos

/interface ethernet set [ find default-name=ether1 ] name=eth1
/interface ethernet set [ find default-name=ether2 ] name=eth2
/interface ethernet set [ find default-name=ether3 ] name=eth3
/interface ethernet set [ find default-name=ether4 ] name=eth4

# Deshabilitamos la wifi

/interface wireless set [ find default-name=wlan1 ] disabled=yes

# Eliminamos servidor y cliente dhcp

/ip pool remove 0
/ip dhcp-server network remove 0
/ip dhcp-server remove 0
/ip dhcp-client remove 0
/ip dns static remove 0
/ip dns set allow-remote-requests=no

# Eliminamos regla de nat masquerade
/ip firewall nat remove [/ip firewall nat find]

# Opcionalmente eliminamos todas las reglas de filter y mangle
# pero hay que ser conscientes que dejaremos el router accesible
# por telnet desde cualqauier puerto
/ip firewall filter remove [/ip firewall filter find]
/ip firewall mangle remove [/ip firewall mangle find]

```

Ahora le cambiamos el nombre y guardamos la configuración inicial por
si la necesitamos restaurar en cualquier momento
```
/system identity set name=mkt00
/system backup save name="20170317_zeroconf"
```

###[ejercicio1] Explica el procedimiento para resetear el router y restaurar
este backup, verifica con /export que no queda ninguna configuración inicial 
que pueda molestarnos para empezar a programar el router
	·Para hacer el backup
	--> /system backup
	--> save name=backup01
	·Para ver donde esta el bakcup abrimos la carpeta file y hacemos un print
	--> /file print
	-->	-# NAME                                                          TYPE                                                               SIZE CREATION-TIME       
	-->-0 skins                                                         directory                                                               jan/01/1970 00:00:01
	-->-1 auto-before-reset.backup                                      backup                                                          18.7KiB jan/02/1970 00:03:06
	-->-2 backup01.backup                                               backup                                                          19.0KiB jan/02/1970 00:18:02
	-->-3 20170317_zeroconf.backup                                      backup                                                          19.1KiB jan/02/1970 00:20:19
    ·Para resetear el router hacemos el comando
    -->system reset                                
	-->Dangerous! Reset anyway? [y/N]: 
	-->y
	-->system configuration will be reset
	Para restaurar el backup hacemos el siguiente comando:
	-->system backup load name=backup01

### Crear interfaces y bridges

Primero hay que pensar en capa 2, vamos a crear un bridge por cada red wifi
que tenga un punto de red cableado asociado a una vlan

Trabajaremos con dos vlans:

*1XX: red pública
*2XX: red privada

Hay que crear unas interfaces virtuales wifi adicional y cambiar los ssids
para reconocerlas cuando escaneemos las wifis

```
/interface wireless set 0 ssid="free1XX"
/interface wireless add ssid="private2XX" master-interface=wlan1
```



###[ejercicio2] Explica por qué aparecen las interfaces wifi como disabled
al hacer /interface print. Habilita y deshabilita estar interfaces. Cambiales
el nombre a wFree wPrivate y dejalas deshabilitadas

Para crear las interfaces

/interface wireless set 0 ssid="free1XX"
/interface wireless add ssid="private2XX" master-interface=wlan1

4  XS wlan1                               wlan             1500  1600       2290 6C:3B:6B:BF:10:DF
5  X  wlan2                               wlan                   1600       2290 6E:3B:6B:BF:10:DF

Habilitar interfaces
[admin@MikroTik] > /interface enable wlan1    
[admin@MikroTik] > /interface enable wlan2

Deshabilitar interfaces
[admin@MikroTik] > /interface disable wlan1    
[admin@MikroTik] > /interface disable wlan2

Para cambiar el nombre a las interfaces se utiliza el siguiente comando:
[admin@MikroTik] > /interface set name=wFree wlan2
[admin@MikroTik] > /interface set name=wPrivate wlan1


## Crear interfaces virtuales y hacer bridges

```
/interface vlan add name eth4-vlan1XX vlan-id=1XX interface=eth4
/interface vlan add name eth4-vlan2XX vlan-id=2XX interface=eth4   

/interface bridge add name=br-vlan1XX
/interface bridge add name=br-vlan2XX


/interface bridge port add interface=eth4-vlan1XX bridge=br-vlan1XX
/interface bridge port add interface=eth3 bridge=br-vlan1XX
/interface bridge port add interface=wFree  bridge=br-vlan1XX      


/interface bridge port add interface=eth4-vlan2XX bridge=br-vlan2XX
/interface bridge port add interface=wPrivate   bridge=br-vlan2XX

/interface print


``` 

###[ejercicio3] Comenta cada una de estas líneas de la configuración
anterior para que quede bien documentado
[admin@MikroTik] > interface print 
Flags: D - dynamic, X - disabled, R - running, S - slave 
 #     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS      
 0     ether1                              ether            1500  1598       2028 6C:3B:6B:BF:10:DB
 1  RS ether2-master                       ether            1500  1598       2028 6C:3B:6B:BF:10:DC
 2   S ether3                              ether            1500  1598       2028 6C:3B:6B:BF:10:DD
 3   S ether4                              ether            1500  1598       2028 6C:3B:6B:BF:10:DE
 4  X  wFree                               wlan                   1600       2290 6E:3B:6B:BF:10:DF
 5  XS wPrivate                            wlan             1500  1600       2290 6C:3B:6B:BF:10:DF
 6  R  br-vlan1XX                          bridge           1500 65535            00:00:00:00:00:00
 7  R  br-vlan2XX                          bridge           1500 65535            00:00:00:00:00:00
 8  R  ;;; defconf
       bridge                              bridge           1500  1598            6C:3B:6B:BF:10:DC


Aqui añadimos dos vlans en un puerto fisico, las vlans son la vlan1xx y la 
vlan2xx y estan assiganadas al puerto 4
#/interface vlan add name eth4-vlan1XX vlan-id=1XX interface=eth4
#/interface vlan add name eth4-vlan2XX vlan-id=2XX interface=eth4  

Aqui hacemos un bridge entre las dos vlans
#/interface bridge add name=br-vlan1XX
#/interface bridge add name=br-vlan2XX

Utilizamos estos comandos para hacer un bridge entre las dos vlans
#/interface bridge port add interface=eth4-vlan1XX bridge=br-vlan1XX
#/interface bridge port add interface=eth3 bridge=br-vlan1XX
#/interface bridge port add interface=wFree  bridge=br-vlan1XX  

### [ejercicio4] Pon una ip 172.17.1XX.1/24 a br-vlan1XX 
y 172.17.2XX.1/24 a br-vlan2XX y verifica desde la interface de la placa 
base de tu ordenador que hay conectividad (puedes hacer ping) configurando
unas ips cableadas. Cuando lo hayas conseguido haz un backup de la configuración
	
	Ponemos ips a ñas interfaces de red del router
	-->[admin@MikroTik] /ip address> ad address=172.17.100.1/24 interface=br-vlan1XX 
	-->[admin@MikroTik] /ip address> ad address=172.17.200.1/24 interface=br-vlan2XX   
	Aqui ponemos una ips de la misma familia en nuestra interfaz de red
	--> ip a a 172.17.100.34 dev enp2s0
	--> ip a a 172.17.200.34 dev enp2s0
	Comprobamos el funcionamiento haciendo pings.
	ping 172.17.100.34
		PING 172.17.100.34 (172.17.100.34) 56(84) bytes of data.
		64 bytes from 172.17.100.34: icmp_seq=1 ttl=64 time=0.035 ms
		64 bytes from 172.17.100.34: icmp_seq=2 ttl=64 time=0.065 ms
		64 bytes from 172.17.100.34: icmp_seq=3 ttl=64 time=0.063 ms
		64 bytes from 172.17.100.34: icmp_seq=4 ttl=64 time=0.066 ms
		64 bytes from 172.17.100.34: icmp_seq=5 ttl=64 time=0.064 ms
	ping 172.17.200.34
		PING 172.17.200.34 (172.17.200.34) 56(84) bytes of data.
		64 bytes from 172.17.200.34: icmp_seq=1 ttl=64 time=0.029 ms
		64 bytes from 172.17.200.34: icmp_seq=2 ttl=64 time=0.064 ms
		64 bytes from 172.17.200.34: icmp_seq=3 ttl=64 time=0.065 ms
		64 bytes from 172.17.200.34: icmp_seq=4 ttl=64 time=0.065 ms
		64 bytes from 172.17.200.34: icmp_seq=5 ttl=64 time=0.065 ms

### [ejercicio5] Crea una servidor dhcp para cada una de las redes en los rangos
.101 a .250 de las respectivas redes. Asignar ip manual a la eth1. 
Crear reglas de nat para salir a internet. 
Crear dhcp:
	Aqui asignamos los rangos de las ips y el nombre 	
		-->[admin@MikroTik] /ip pool>  add name=range_public ranges=172.17.100.101-172.17.100.250
		-->[admin@MikroTik] /ip pool>  add name=range_private ranges=172.17.200.101-172.17.200.250
		-->[admin@MikroTik] /ip> dhcp-server add interface=br-vlan1XX address-pool=range_public 
		-->[admin@MikroTik] /ip> dhcp-server add interface=br-vlan2XX address-pool=range_private 
	Asignamos los rangos a las interfaces
		-->[admin@MikroTik] /ip> dhcp-server print 
			Flags: X - disabled, I - invalid 
			#   NAME    INTERFACE    RELAY           ADDRESS-POOL    LEASE-TIME ADD-ARP
			1 X dhcp2   br-vlan1XX                   range_public    10m       
			2 X dhcp3   br-vlan2XX                   range_private   10m  
	


### [ejercicio6] Activar redes wifi y dar seguridad wpa2
[admin@MikroTik] /interface wireless> /interface wireless enable WFree
[admin@MikroTik] /interface wireless> /interface wireless enable WPrivate


### [ejercicio6b] Opcional (monar portal cautivo)
Para crear el portal cautivo, entramos en nuestra microtik y ponemos el siguiente comando:
 -->[admin@MikroTik] > ip hotspot setup  
Aqui escogemos la interfaz que queramos para hacer el portal cautivo
 -->hotspot interface: WFree
Aqui le ponemos una ip de red y le damos a masquerade yes.
 -->local address of network: 172.17.0.1/24      
 -->masquerade network: yes
Aqui escogemos el rango de ips para el portal cautivo
 -->address pool of network: 172.17.0.2-172.17.0.254


### [ejercicio7] Firewall para evitar comunicaciones entre las redes Free y Private. Limitar
puertos en Free.


### [ejercicio8] Conexión a switch cisco con vlans

### [ejercicio8b] (Opcional) Montar punto de acceso adicional con un AP de otro fabricante

### [ejercicio9] QoS

### [ejercicio9] Balanceo de salida a internet

### [ejercicio10] Red de servidores con vlan de servidores y redirección de puertos

### [ejercicio11] Firewall avanzado

